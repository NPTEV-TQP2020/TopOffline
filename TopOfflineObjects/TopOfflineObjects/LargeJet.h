/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef _LARGEJET_H_
#define _LARGEJET_H_

#include "TLorentzVector.h"
#include "TopOfflineObjects/Particle.h"
#include "TopOfflineObjects/Containers.h"

namespace TopOffline{

  class LargeJet : public Particle{
    
  public:
    LargeJet(float pt, float eta, float phi, float e);
    LargeJet();
    virtual ~LargeJet();

    virtual LargeJet* clone() const;
    virtual LargeJet* deepClone() const;

  private:

  };

  typedef Container<LargeJet> LargeJetContainer;
}


#endif
