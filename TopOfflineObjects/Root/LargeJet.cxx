/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "TopOfflineObjects/LargeJet.h"

namespace TopOffline{

  LargeJet::LargeJet(){
    m_objectType=PhysObjectType::LargeJetType;
  }
  
  LargeJet::LargeJet(float pt, float eta, float phi, float e){
    this->SetPtEtaPhiE(pt,eta,phi,e);
    m_objectType=PhysObjectType::LargeJetType;
  }


  LargeJet::~LargeJet(){
  }

  LargeJet* LargeJet::clone() const{
    return new LargeJet(*this);
  }

  LargeJet* LargeJet::deepClone() const{
    return new LargeJet(*this);
  }

}
