# Auto-generated on: 2017-11-16 17:46:44.211230

# Declare the name of this package:
atlas_subdir( TopOfflineConfiguration None )

# This package depends on other packages:
atlas_depends_on_subdirs( PUBLIC
                          TopOfflineObjects
			  Tools/PathResolver )

# This package uses ROOT:
find_package( ROOT REQUIRED COMPONENTS Core Gpad Tree Hist RIO MathCore Graf Physics )

# Build a library that other components can link against:
atlas_add_library( TopOfflineConfiguration Root/*.cxx Root/*.h Root/*.icc
                   TopOfflineConfiguration/*.h TopOfflineConfiguration/*.icc TopOfflineConfiguration/*/*.h
                   TopOfflineConfiguration/*/*.icc 
                   PUBLIC_HEADERS TopOfflineConfiguration
                   LINK_LIBRARIES TopOfflineObjects
		   		  PathResolver
                                  ${ROOT_LIBRARIES}
                   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} )

