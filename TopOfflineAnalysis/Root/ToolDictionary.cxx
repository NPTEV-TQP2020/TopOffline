/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "TopOfflineAnalysis/ToolDictionary.h"
#include "TopOfflineObjects/TopOfflineUtils.h"
#include "TopOfflineToolManager/DictBase.h"
#include "TopOfflineConfiguration/GlobalConfiguration.h"

#include <string>
#include <vector>
#include "TClass.h"
#include "TSystem.h"

namespace TopOffline{

  void ToolDictionary::buildDictionary(TopOffline::ToolManager* toolManager){
    /// Add each tool here using addToolToDict
    /// It is important to make sure everything is added in running order

    /*toolManager->addToolToDict("Test.runTool",
			       "MyToolsName",
			       []{ return new test("InternalToolName"); }
			       );*/

    /// This is where additional dictionaries from external packages can be loaded
    /// This means that the number of dependencies in OfflineTopOffline is reduced, and
    /// the user/developer needs not modify the OfflineTopOffline package to add in a tool
    /// Additional dictionaries are specified in the config file, giving their full name
    /// For example, TopOffline::ExtraDictionary
    auto* config = TopOffline::GlobalConfiguration::get();
    std::vector<std::string> extralibs = TopOffline::util::vectoriseString((*config)("Dictionaries"));

    
    for(auto lib : extralibs){
      std::cout << "INFO: Adding tools to the ToolManager from Dictionary in  " << lib << std::endl;
      gSystem->Load(("lib" + lib + ".so").c_str());
      TClass* c = TClass::GetClass(("TopOffline::" + lib + "Dictionary").c_str());
      if(c == 0){
	std::cout << "ERROR: Library for dictionary in " << lib << " not found! Check the naming is TopOffline::" << lib << "Dictionary." << std::endl;
	exit(18);
      }
      DictBase* dict = (DictBase*)c->New();

      dict->buildDictionary(toolManager);
      delete dict;
      delete c;
    }

    toolManager->dictionaryLoaded();
  }

  ToolDictionary::ToolDictionary(){
  }

  ToolDictionary::~ToolDictionary(){
  }

}
