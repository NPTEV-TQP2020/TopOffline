**Instructions to set-up TopOffline**
------------

Useful links:

  * https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopRun2WorkshopTutorial2018
  * https://gitlab.cern.ch/atlasHTop/TTHbbAnalysis

First time you setup:
      
      mkdir AnalysisTop_21.2.97/
      cd AnalysisTop_21.2.97/
      mkdir build run
      echo 'export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase' > settings.sh
      echo 'source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh' >> settings.sh
      echo 'export ROOTCORE_NCPUS="$(nproc)"' >> settings.sh
      source settings.sh
      git clone ssh://git@gitlab.cern.ch:7999/NPTEV-TQP2020/TopOffline.git
      cd build/
      asetup AnalysisTop,21.2.97
      cmake -DATLAS_PACKAGE_FILTER_FILE=../TopOffline/PackageFilters/offline_packages.txt ../TopOffline/  
      cmake --build ./
      source */setup.sh
      cd ..
      export LD_LIBRARY_PATH=`pwd`/build/x86_64-slc6-gcc62-opt/lib:$LD_LIBRARY_PATH
      
Any other time, for setup:
      
      cd AnalysisTop_21.2.97/
      source settings.sh
      cd build
      asetup  --restore
      source */setup.sh
      cd ..
      export LD_LIBRARY_PATH=`pwd`/build/x86_64-slc6-gcc62-opt/lib:$LD_LIBRARY_PATH
 
